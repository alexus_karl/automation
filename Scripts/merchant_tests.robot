*** Settings ***
Library      SeleniumLibrary

*** Variables ***
${EMAIL}     dpuseermerchant@yahoo.com
${PASSWORD}  Password@2022
${BROWSER}   chrome
${URL}       https://appsvc-backoffice-osler-001.azurewebsites.net/onboard/login
${EMAIL_BTN}     xpath:/html/body/div[1]/div/div/div[2]/div/div[5]/div/a/div
${EMAIL_FIELD}   id:email
${PASSWORD_FIELD}    id:password
${LOGIN_BTN}     xpath:/html/body/div[1]/div/div/div/div[2]/div/form/div[3]/div/div
${MWELL_ICON}    xpath:/html/body/div[1]/div/div[4]/div/div[1]/div


##DASHBOARD
${SET_DATE_BTN}  xpath:/html/body/div[1]/div/div[5]/div[2]/div[1]/div[3]/div[1]/div[2]/div[4]
${START_DATE_FIELD}  xpath:/html/body/div[1]/div/div[2]/div/div/div[2]/div[1]/div[2]/div[1]/div[1]/div/input
${END_DATE_FIELD}    xpath:/html/body/div[1]/div/div[2]/div/div/div[2]/div[3]/div[2]/div[1]/div[1]/div/input
${START_DATE}     02/28/2024
${END_DATE}       02/29/2024
${SAVE_DATE_BTN}     xpath:/html/body/div[1]/div/div[2]/div/div/div[4]/div[1]/span
${NOWHERE}      xpath:/html/body/div[1]/div/div[2]/div/div

*** Test Cases ***
MM-001-001
    [Documentation]  Verify that the user can login as Merchant role
    OPEN BROWSER     ${URL}  ${BROWSER}
    MAXIMIZE BROWSER WINDOW
    Scroll Element Into View     ${EMAIL_BTN}
    CLICK ELEMENT    ${EMAIL_BTN}
    INPUT TEXT       ${EMAIL_FIELD}  ${EMAIL}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK ELEMENT    ${LOGIN_BTN}
    SLEEP   5s
    ELEMENT SHOULD BE VISIBLE    ${MWELL_ICON}

MM-001-002
    [Documentation]  Verify that the user can filter for specific date range when clicking "Set" button
    CLICK ELEMENT    ${SET_DATE_BTN}
    INPUT TEXT   ${START_DATE_FIELD}     ${START_DATE}
    INPUT TEXT   ${END_DATE_FIELD}     ${END_DATE}
    CLICK ELEMENT    ${NOWHERE}
    SLEEP  3s
    CLICK ELEMENT    ${SAVE_DATE_BTN}
    SLEEP  3s
