*** Settings ***
Library      SeleniumLibrary

*** Variables ***
${EMAIL}     marketforger@gmail.com
${PASSWORD}  Market_123
${BROWSER}   chrome
${URL}       https://appsvc-backoffice-osler-002.azurewebsites.net/
${EMAIL_BTN}     xpath:/html/body/div[1]/div/div/div[2]/div/div[5]/div/a/div
${EMAIL_FIELD}   id:email
${PASSWORD_FIELD}    id:password
${LOGIN_BTN}     xpath:/html/body/div[1]/div/div/div/div[2]/div/form/div[3]/div/div
${MWELL_ICON}    xpath:/html/body/div[1]/div/div[4]/div/div[1]/div


*** Test Cases ***
PP-001-005
    [Documentation]  Login using Patient
    OPEN BROWSER     ${URL}  ${BROWSER}
    MAXIMIZE BROWSER WINDOW
    CLICK ELEMENT    ${EMAIL_BTN}
    INPUT TEXT       ${EMAIL_FIELD}  ${EMAIL}
    INPUT PASSWORD   ${PASSWORD_FIELD}   ${PASSWORD}
    CLICK ELEMENT    ${LOGIN_BTN}
    SLEEP   5s
    ELEMENT SHOULD BE VISIBLE    ${MWELL_ICON}



